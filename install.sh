#!/usr/bin/env bash

mkdir -p ~/.ssh/

ln -s  /home/chris/dotfiles/bash_aliases ~/.bash_aliases
ln -s  /home/chris/dotfiles/bash_profile ~/.bash_profile
ln -fs /home/chris/dotfiles/bashrc       ~/.bashrc
ln -s  /home/chris/dotfiles/gitconfig    ~/.gitconfig
ln -s  /home/chris/dotfiles/ssh_config   ~/.ssh/config
ln -s  /home/chris/dotfiles/tmux.conf    ~/.tmux.conf
ln -s  /home/chris/dotfiles/vimrc        ~/.vimrc
